<?php
// Inicio
Breadcrumbs::for('index', function ($trail) {
    $trail->push(AppHelper::site()->lang('title.index'), route('index'));
});

// Inicio > Admin
Breadcrumbs::for('admin.index', function ($trail) {
    $trail->parent('index');
    $trail->push('Panel de Administración', route('admin.index'));
});
Breadcrumbs::for('admin.notices', function ($trail) {
    $trail->parent('admin.index');
    $trail->push('Noticias', route('admin.notices'));
});
Breadcrumbs::for('admin.notices.create', function ($trail) {
    $trail->parent('admin.notices');
    $trail->push('Crear Noticia', route('admin.notices.create'));
});

//Inicio > User
Breadcrumbs::for('user.index', function ($trail) {
    $trail->parent('index');
    $trail->push('Panel de usuario', route('user.index'));
});

//Inicio > Map
Breadcrumbs::for('maps.index', function ($trail) {
    $trail->parent('index');
    $trail->push('Mapas', route('maps.index'));
});
Breadcrumbs::for('maps.shops', function ($trail) {
    $trail->parent('maps.index');
    $trail->push('Mapa de los negocios', route('maps.shops'));
});
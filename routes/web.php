<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('index');

Route::namespace('Admin')->group(function () {
  Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::prefix('notices')->group(function () {
      Route::get('/', 'NoticeController@index')->name('admin.notices');
      Route::get('create', 'NoticeController@create')->name('admin.notices.create');
    });
  });
});

Route::namespace('User')->group(function () {
	Route::prefix('user')->group(function () {
		Route::get('/', 'UserController@index')->name('user.index');
	});
});

Route::namespace('Maps')->group(function () {
	Route::prefix('maps')->group(function () {
		Route::get('/', 'MapsController@index')->name('maps.index');
		Route::prefix('shops')->group(function () {
			Route::get('/', 'ShopsController@index')->name('maps.shops');
		});
	});
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

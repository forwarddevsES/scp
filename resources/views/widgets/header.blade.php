<div class="hero-unit">
    <div class="container hero-unit__container">
        <div class="hero-unit__content hero-unit__content--left-center"><span class="hero-unit__decor"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
            <h5 class="hero-unit__subtitle">{{AppHelper::site()->setting('header.subtitle')}}</h5>
            <h1 class="hero-unit__title">{{AppHelper::site()->setting('header.title')}}<span class="text-primary">{{AppHelper::site()->setting('header.titlecolor')}}</span></h1>
            <div class="hero-unit__desc">{{AppHelper::site()->setting('header.description')}}</div>
            @if(AppHelper::site()->setting('header.button.active'))
              <a href="{{AppHelper::site()->setting('header.button.href')}}"class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed hero-unit__btn">{{AppHelper::site()->setting('header.button.text')}}</a>
            @endif
        </div>
        <figure class="hero-unit__img"><img src="{{asset('assets\images\samples\header_player.png')}}" alt="Personaje"></figure>
    </div>
</div>

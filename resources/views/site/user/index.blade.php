@extends('layouts.site')
@section('title', AppHelper::site()->lang('title.index'))
@section('header')
{{ Breadcrumbs::render('user.index') }}
@endsection
@section('content')
<div class="site-content">
    <div class="container">
        <div class="row">
            <div class="col-12"">
	            <table class="table">
	            	<thead>
	            		<tr>
	            			<th>Stats de tu personaje</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		<tr>
	            			<td>Nombre y Apellido</td>
	            			<td>{{ $user->name }}_{{ $user->last_name }}</td>
	            		</tr>
	            		<tr>
	            			<td>Email</td>
	            			<td>{{ $user->email }}</td>
	            		</tr>
	            		<tr>
	            			<td>Nivel</td>
	            			<td>{{ $user->level }}</td>
	            		</tr>
	            		<tr>
	            			<td>Dinero</td>
	            			<td>{{ $user->money }}</td>
	            		</tr>
	            		<tr>
	            			<td>Asesinatos</td>
	            			<td>{{ $user->kills }}</td>
	            		</tr>
	            		<tr>
	            			<td>Muertes</td>
	            			<td>{{ $user->deads }}</td>
	            		</tr>
	            	</tbody>
	            </table>
            </div>
        </div>
    </div>
</div>
@endsection

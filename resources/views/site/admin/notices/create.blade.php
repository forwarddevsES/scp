@extends('layouts.site')
@section('styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendor/jodit/jodit.min.css')}}">
@endsection
@section('title', AppHelper::site()->lang('title.index'))
@section('header')
{{ Breadcrumbs::render('admin.notices.create') }}
@endsection
@section('content')
<div class="site-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 order-md-2">
                <div class="card">
                    <header class="card__header">
                        <h4>Crear una Noticia</h4>
                    </header>
                    <center>
                        <div class="card__content">
                            <form action="#" enctype="multipart/form-data" class="contact-form">
                                <div class="form-group">
                                    <label for="title">Portada 378x270 <span class="required">*</span></label>
                                    <input type="file" name="image" id="image" class="form-control" accept="image/*" placeholder="Ingresa una imagen valida de 378x270...">
                                </div>
                                <div class="form-group">
                                    <label for="title">Titulo <span class="required">*</span></label>
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Ingresa el titulo de la noticia...">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="category">Categoría <span class="required">*</span></label>
                                    <select name="category" id="category" class="form-control" required>
                                        <option value="1">Servidor</option>
                                        <option value="2">Información</option>
                                        <option value="3">Notas de versión</option>
                                        <option value="4">Web</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">Contenido <span class="required">*</span></label>
                                    <textarea id="editor" name="content"></textarea>
                                </div>
                                <div class="form-group form-group--submit"><button type="submit" class="btn btn-primary-inverse btn-lg btn-block">Crear Noticia</button></div>
                            </form>
                        </div>
                    </center>
                </div>
            </div>
            @include('site.admin.partials.sidebar')
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('assets/vendor/jodit/jodit.min.js')}}"></script>
<script type="text/javascript">
    var editor = new Jodit('#editor');
    editor.value = '<p>¡Comienza a escribir tu noticia!</p>';
</script>
@endsection

@extends('layouts.site')
@section('title', AppHelper::site()->lang('title.index'))
@section('header')
{{ Breadcrumbs::render('admin.notices') }}
@endsection
@section('content')
<div class="site-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 order-md-2">
                <div class="card card--has-table">
                    <div class="card__header">
                        <h4 style="font-size: 20px;">Lista de noticias <div class="pull-right"><a href="#" class="btn btn-primary btn-xs">Crear Noticia</a></div></h4> 
                    </div>
                    <div class="card__content">
                        <div class="table-responsive">
                            <table class="table shop-table">
                                <thead>
                                    <tr>
                                        <th class="product__photo">Portada</th>
                                        <th class="product__info">Información</th>
                                        <th class="product__info">Fecha</th>
                                        <th class="product__remove">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach ($notices as $notice)
                                    <tr>
                                        <td class="product__photo">
                                            <figure class="product__thumb"><a href="#"><img src="{{asset($notice->image)}}" alt=""></a></figure>
                                        </td>
                                        <td class="product__info"><span class="product__cat">{{$notice->category}}</span>
                                            <h5 class="product__name"><a href="#">{{$notice->title}}</a></h5>
                                            <div class="product__ratings"><i class="meta-like icon-heart"></i> 530</div>
                                        </td>
                                        <td class="product__info">{{$notice->created_at}}</td>
                                        <td class="product__remove"><a href="#" class="fa fa-times product__remove-icon"></a></td>
                                    </tr>
                                  @endforeach
                                  @if ($notices->count() == 0)
                                    <td colspan="4" style="text-align:center;"><strong>Al parecer no hay nada para mostrar, intenta crear una noticia.</strong></td>
                                  @endif

                                </tbody>
                            </table>

                            <nav aria-label="Navegación" class="post__comments-pagination">
                                {{ $notices->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @include('site.admin.partials.sidebar')
        </div>
    </div>
</div>
@endsection

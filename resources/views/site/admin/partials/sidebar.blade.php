<div class="sidebar sidebar--shop col-md-3 order-md-1">
    <aside class="widget card widget--sidebar widget_categories">
        <div class="widget__title card__header">
            <h4>Modulos</h4>
        </div>
        <div class="widget__content card__content">
            <ul class="widget__list">
                <li class="active"><a href="{{route('admin.index')}}">Resumen</a></li>
                <li><a href="{{route('admin.notices')}}">Noticias</a></li>
                <li class="has-children"><a href="#">Ajustes</a>
                    <ul>
                        <li><a href="#">General</a></li>
                        <li><a href="#">Lenguaje</a></li>
                        <li><a href="#">Modulos</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </aside>
</div>

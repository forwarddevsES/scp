@extends('layouts.site')
@section('title', AppHelper::site()->lang('title.index'))
@section('header')
{{ Breadcrumbs::render('admin.index') }}
@endsection
@section('content')
<div class="site-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 order-md-2">

            </div>
            @include('site.admin.partials.sidebar')
        </div>
    </div>
</div>
@endsection

@extends('layouts.site')
@section('title', AppHelper::site()->lang('title.index'))
@section('header')
  @if(AppHelper::site()->setting('header.active') == 1)
    @include('widgets.header')
  @endif
@endsection
@section('content')
    <div class="site-content">
        <div class="container">
            <div class="row">
                <div class="content col-md-8">
                    <article class="card card--lg card--block post post--single">
                        <figure class="post__thumbnail"><img src="{{asset($notice->image)}}" alt=""></figure>
                        <div class="post__meta-block post__meta-block--side">
                            <div class="post-author">
                                <figure class="post-author__avatar"><img src="{{asset('assets\images\samples\avatar-1.jpg')}}" alt="Post Author Avatar"></figure>
                                <div class="post-author__info">
                                    <h4 class="post-author__name">{{$notice->user->name}}</h4><span class="post-author__slogan">Administrador</span>
                                </div>
                            </div>

                            <ul class="post__meta meta">
                                <li class="meta__item meta__item--views">1</li>
                                <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 0</a></li>
                                <li class="meta__item meta__item--comments"><a href="#">0</a></li>
                            </ul><!-- Post Meta Info / End -->
                        </div><!-- Post Meta - Side / End -->
                        <div class="card__content">
                            <div class="post__category"><span class="label posts__cat-label">{{$notice->categoryName()}}</span></div>
                            <header class="post__header">
                                <h2 class="post__title">{{$notice->title}}</h2>
                                <ul class="post__meta meta">
                                    <li class="meta__item meta__item--date"><time datetime="2017-08-23">{{$notice->created_at}}</time></li>

                                </ul>
                            </header>
                            <div class="post__content">
                                {!! $notice->content !!}
                            </div>

                        </div>
                    </article>
                    @if ($others->count() == 2)

                    @endif
                    <div class="post-related">
                        <div class="card">
                            <div class="card__header">
                                <h4>Noticias parecidas</h4>
                            </div>
                        </div>
                        <div class="row posts--cards">
                            @foreach ($others as $other)
                              <div class="col-md-6">
                                  <div class="posts__item posts__item--card posts__item--category-2 card">
                                      <figure class="posts__thumb">
                                          <div class="posts__cat"><span class="label posts__cat-label">{{$other->categoryName()}}</span></div><a href="#"><img src="{{asset($other->image)}}" alt=""></a>
                                      </figure>
                                      <div class="posts__inner card__content"><time datetime="{{$other->created_at}}" class="posts__date">{{$other->created_at}}</time>
                                          <h6 class="posts__title"><a href="#">{{$other->title}}</a></h6>
                                      </div>
                                      <footer class="posts__footer card__footer">
                                          <div class="post-author">
                                              <figure class="post-author__avatar"><img src="{{asset('assets\images\samples\avatar-2.jpg')}}" alt="Post Author Avatar"></figure>
                                              <div class="post-author__info">
                                                  <h4 class="post-author__name">{{$other->user->name}}</h4>
                                              </div>
                                          </div>
                                          <ul class="post__meta meta">
                                              <li class="meta__item meta__item--views">0</li>
                                              <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 0</a></li>
                                              <li class="meta__item meta__item--comments"><a href="#">0</a></li>
                                          </ul>
                                      </footer>
                                  </div><!-- Prev Post / End -->
                              </div>
                            @endforeach

                        </div>
                    </div><!-- Related Posts / End -->
                    <!-- Post Comments -->
                    <div class="post-comments card card--lg">
                        <header class="post-commments__header card__header">
                            <h4>Comentarios ({{$notice->comments->count()}})</h4>
                        </header>
                        <div class="post-comments__content card__content">
                            <ul class="comments">
                                @foreach ($comments as $comment)
                                  <li class="comments__item">
                                      <div class="comments__inner">
                                          <header class="comment__header">
                                              <div class="comment__author">
                                                  <figure class="comment__author-avatar"><img src="{{asset('assets\images\samples\avatar-10.jpg')}}" alt=""></figure>
                                                  <div class="comment__author-info">
                                                      <h5 class="comment__author-name">{{$comment->user->name}}</h5><time class="comment__post-date" datetime="2016-08-23">hace 5 minutos</time>
                                                  </div>
                                              </div>
                                              <div class="comment__reply"><a href="#" class="comment__reply-link btn btn-link btn-xs">Reply</a></div>
                                          </header>
                                          <div class="comment__body">{{$comment->comment}}</div>
                                      </div>
                                  </li>
                                @endforeach

                                @auth
                                  <li class="comments__item">
                                      <div class="comments__inner">
                                          <header class="comment__header">
                                              <div class="comment__author">
                                                  <figure class="comment__author-avatar"><img src="{{asset('assets\images\samples\avatar-10.jpg')}}" alt=""></figure>
                                                  <div class="comment__author-info">
                                                      <h5 class="comment__author-name">{{auth()->user()->name}}</h5><time class="comment__post-date" datetime="2016-08-23">¡Comenta!</time>
                                                  </div>
                                              </div>
                                          </header>
                                          <form action="#" class="comment-form">
                                              <div class="form-group"><textarea name="comment" id="comment" rows="3" class="form-control" placeholder="Deja un comentario..."></textarea></div>
                                              <div class="form-group"><button type="submit" class="btn btn-default btn-block btn-lg">Comentar</button></div>
                                          </form>
                                      </div>

                                  </li>
                                @else
                                  <li class="comments__item">
                                      <div class="comments__inner">
                                          <h6><a href="{{route('login')}}">Ingresa</a> o <a href="{{route('register')}}">Registrate</a> para dejar un comentario.</h6>
                                      </div>
                                  </li>
                                @endauth
                            </ul>
                            <nav aria-label="Comments Pavigation" class="post__comments-pagination" aria-label="Comments navigation">
                              {{$comments->links()}}
                            </nav>
                        </div>
                    </div>
                </div>
                <div id="sidebar" class="sidebar col-md-4">

                    @auth
                      <aside class="widget card widget--sidebar widget-player">
                          <div class="widget__content card__content">
                              <div class="widget-player__team-logo"><img src="{{asset('assets\images\logo.png')}}" alt=""></div>
                              <figure class="widget-player__photo"><img src="{{asset('assets\images\samples\boys-3.png')}}" alt=""></figure>
                              <header class="widget-player__header clearfix">
                                  <div class="widget-player__number">#1</div>
                                  <h4 class="widget-player__name"><span class="widget-player__first-name">Jim</span> <span class="widget-player__last-name">Street</span></h4>
                              </header>
                              <div class="widget-player__content">
                                  <div class="widget-player__content-inner">
                                      <div class="widget-player__stat widget-player__assists">
                                          <h6 class="widget-player__stat-label">Assists</h6>
                                          <div class="widget-player__stat-number">16.9</div>
                                          <div class="widget-player__stat-legend">AVG</div>
                                      </div>
                                      <div class="widget-player__stat widget-player__steals">
                                          <h6 class="widget-player__stat-label">Steals</h6>
                                          <div class="widget-player__stat-number">7.2</div>
                                          <div class="widget-player__stat-legend">AVG</div>
                                      </div>
                                      <div class="widget-player__stat widget-player__blocks">
                                          <h6 class="widget-player__stat-label">Blocks</h6>
                                          <div class="widget-player__stat-number">12.4</div>
                                          <div class="widget-player__stat-legend">AVG</div>
                                      </div>
                                  </div>
                              </div>
                              <footer class="widget-player__footer"><span class="widget-player__footer-txt"><i class="fa fa-star"></i> Mejor Jugador</span></footer>
                          </div>
                      </aside>
                    @else
                      <aside class="widget card widget--sidebar">
                        <div class="widget__title card__header">
                            <h4>Login</h4>
                        </div>
                        <div class="widget__content card__content">
                          <form method="POST" action="{{ route('login') }}" class="modal-form">
                              <h5>Ingresa en tu cuenta</h5>
                              @csrf
                              <div class="form-group"><input type="email" name="email" class="form-control" placeholder="Ingresa tu dirección de correo..." value="{{ old('email') }}" required autofocus></div>
                              <div class="form-group"><input type="password" name="password" class="form-control" placeholder="Ingresa tu contraseña..." required></div>
                              <div class="form-group form-group--pass-reminder"><label class="checkbox checkbox-inline">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame <span class="checkbox-indicator"></span></label>
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
                                @endif
                              </div>
                              @if(AppHelper::site()->setting('site.recaptcha.active') == 1)
                                <div class="g-recaptcha" data-sitekey="{{AppHelper::site()->setting('site.recaptcha.site_key')}}"></div>
                              @endif
                              <div class="form-group form-group--submit"><button type="submit" class="btn btn-primary-inverse btn-block">Ingresar</button></div>


                          </form>
                        </div>
                      </aside>
                    @endauth
                    <aside class="widget widget--sidebar widget-social">
                      @if (AppHelper::site()->setting('site.facebook.active'))
                        <a href="{{AppHelper::site()->setting('site.facebook.url')}}" class="btn-social-counter btn-social-counter--fb" target="_blank">
                              <div class="btn-social-counter__icon"><i class="fa fa-facebook"></i></div>
                              <h6 class="btn-social-counter__title">SIGUENOS EN FACEBOOK</h6><span class="btn-social-counter__count"> CLICK AQUÍ</span> <span class="btn-social-counter__add-icon"></span>
                          </a>
                      @endif
                      @if (AppHelper::site()->setting('site.twitter.active'))
                        <a href="{{AppHelper::site()->setting('site.twitter.url')}}" class="btn-social-counter btn-social-counter--twitter" target="_blank">
                            <div class="btn-social-counter__icon"><i class="fa fa-twitter"></i></div>
                            <h6 class="btn-social-counter__title">SIGUENOS EN TWITTER</h6><span class="btn-social-counter__count"> CLICK AQUÍ</span> <span class="btn-social-counter__add-icon"></span>
                        </a>
                      @endif
                      @if (AppHelper::site()->setting('site.instagram.active'))
                        <a href="{{AppHelper::site()->setting('site.instagram.url')}}" class="btn-social-counter btn-social-counter--instagram" target="_blank">
                            <div class="btn-social-counter__icon"><i class="fa fa-instagram"></i></div>
                            <h6 class="btn-social-counter__title">SIGUENOS EN INSTAGRAM</h6><span class="btn-social-counter__count"> CLICK AQUÍ</span> <span class="btn-social-counter__add-icon"></span>
                        </a>
                      @endif

                    </aside>

                    <aside class="widget widget--sidebar card widget-tabbed">
                        <div class="widget__title card__header">
                            <h4>Otras Noticias</h4>
                        </div>
                        <div class="widget__content card__content">
                            <div class="widget-tabbed__tabs">
                                <!-- Widget Tabs -->
                                <ul class="nav nav-tabs nav-justified widget-tabbed__nav" role="tablist">
                                    <li class="nav-item"><a href="#widget-tabbed-newest" class="nav-link active" aria-controls="widget-tabbed-newest" role="tab" data-toggle="tab">Newest</a></li>
                                    <li class="nav-item"><a href="#widget-tabbed-commented" class="nav-link" aria-controls="widget-tabbed-commented" role="tab" data-toggle="tab">Most Commented</a></li>
                                    <li class="nav-item"><a href="#widget-tabbed-popular" class="nav-link" aria-controls="widget-tabbed-popular" role="tab" data-toggle="tab">Popular</a></li>
                                </ul><!-- Widget Tab panes -->
                                <div class="tab-content widget-tabbed__tab-content">
                                    <!-- Newest -->
                                    <div role="tabpanel" class="tab-pane fade show active" id="widget-tabbed-newest">
                                        <ul class="posts posts--simple-list">
                                            <li class="posts__item posts__item--category-1">
                                              <figure class="posts__thumb"><a href="#"><img src="{{asset('assets\images\samples\post-img1-xs.jpg')}}" alt=""></a></figure>
                                                <div class="posts__inner">
                                                    <div class="posts__cat"><span class="label posts__cat-label">The Team</span></div>
                                                    <h6 class="posts__title"><a href="#">Jake Dribbler Announced that he is retiring next month</a></h6><time datetime="2016-08-23" class="posts__date">August 23rd, 2018</time>
                                                </div>
                                            </li>

                                        </ul>
                                    </div><!-- Commented -->
                                    <div role="tabpanel" class="tab-pane fade" id="widget-tabbed-commented">
                                        <ul class="posts posts--simple-list">
                                          <li class="posts__item posts__item--category-1">
                                            <figure class="posts__thumb"><a href="#"><img src="{{asset('assets\images\samples\post-img1-xs.jpg')}}" alt=""></a></figure>
                                              <div class="posts__inner">
                                                  <div class="posts__cat"><span class="label posts__cat-label">Categoria</span></div>
                                                  <h6 class="posts__title"><a href="#">Titulo</a></h6><time datetime="2016-08-23" class="posts__date">August 23rd, 2018</time>
                                              </div>
                                          </li>
                                        </ul>
                                    </div><!-- Popular -->
                                    <div role="tabpanel" class="tab-pane fade" id="widget-tabbed-popular">
                                        <ul class="posts posts--simple-list">
                                          <li class="posts__item posts__item--category-1">
                                            <figure class="posts__thumb"><a href="#"><img src="{{asset('assets\images\samples\post-img1-xs.jpg')}}" alt=""></a></figure>
                                              <div class="posts__inner">
                                                  <div class="posts__cat"><span class="label posts__cat-label">The Team</span></div>
                                                  <h6 class="posts__title"><a href="#">Jake Dribbler Announced that he is retiring next month</a></h6><time datetime="2016-08-23" class="posts__date">August 23rd, 2018</time>
                                              </div>
                                          </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                    @if (AppHelper::site()->setting('announce.active') == 1)
                      <aside class="widget card widget--sidebar widget-banner">
                          <div class="widget__title card__header">
                              <h4>Anuncios</h4>
                          </div>
                          <div class="widget__content card__content">
                              <figure class="widget-banner__img">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                  </ol>
                                  <div class="carousel-inner">
                                    @foreach ($advertising as $ad)
                                      @if ($ad->active == 1)
                                        <div class="carousel-item active">
                                          <a href="{{ $ad->link }}">
                                            <img class="d-block w-100" src="{{ $ad->image }}">
                                          </a>
                                        </div>
                                      @else
                                        <div class="carousel-item">
                                          <a href="{{ $ad->link }}">
                                            <img class="d-block w-100" src="{{ $ad->image }}">
                                          </a>
                                        </div>
                                      @endif
                                    @endforeach
                                  </div>
                                </div>
                              </figure>
                          </div>
                      </aside>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
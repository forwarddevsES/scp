
<!DOCTYPE html>
<html lang="es">

<head>
	<title>@yield('title') - {{AppHelper::site()->setting('site.name')}}</title>
	<meta charset="utf-8">
    <base href="{{ env('APP_URL') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="{{AppHelper::site()->setting('site.description')}}">
	<meta name="author" content="ForwardDevs">
	<meta name="keywords" content="{{AppHelper::site()->setting('site.keywords')}}">
	<link rel="shortcut icon" href="{{asset('assets/images/basketball/favicons/favicon.ico')}}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/images/basketball/favicons/favicon-120.png')}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/images/basketball/favicons/favicon-152.png')}}">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">

	<link href="{{asset('https://fonts.googleapis.com/css?family=Montserrat:400,700%7CSource+Sans+Pro:400,700')}}" rel="stylesheet">

	<link href="{{asset('assets/vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/fonts/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
	<link href="{{asset('assets/vendor/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
	<link href="{{asset('assets/vendor/slick/slick.css')}}" rel="stylesheet"><!-- Template CSS-->
	<link href="{{asset('assets/css/style-basketball.css')}}" rel="stylesheet"><!-- Custom CSS-->
	<link href="{{asset('assets/css/custom.css')}}" rel="stylesheet">
    {{-- Custom Styles --}}
    @yield('styles')

</head>

<body class="template-basketball">
	<div class="site-wrapper clearfix">
		<div class="site-overlay"></div>

		@include('partials.navbar')

		@include('partials.aside')

		@yield('header')

		@yield('content')

        @include('partials.footer')

		@include('modals.loginModal')
	</div>

	<script src="{{asset('assets\vendor\jquery\jquery.min.js')}}"></script>
	<script src="{{asset('assets\vendor\bootstrap\js\bootstrap.bundle.js')}}"></script>
	<script src="{{asset('assets\js\core.js')}}"></script><!-- Vendor JS -->
	<script src="{{asset('assets\vendor\twitter\jquery.twitter.js')}}"></script><!-- Template JS -->
	<script src="{{asset('assets\js\init.js')}}"></script>
	<script src="{{asset('assets\js\custom.js')}}"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
    {{-- Custom Scripts --}}
    @yield('scripts')
</body>

</html>

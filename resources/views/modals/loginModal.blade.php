<div class="modal fade" id="loginModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal--login" role="document">
        <div class="modal-content">
            <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
            <div class="modal-body">
                <div class="modal-account-holder">
                    <div class="modal-account__item">
                        <form method="POST" action="{{ route('register') }}" class="modal-form">
                            @csrf
                            <h5>Crea una cuenta</h5>
                            <div class="form-group"><input type="text" class="form-control" placeholder="Ingresa un nombre de usuario..." name="name" value="{{ old('name') }}" required autofocus></div>
                            <div class="form-group"><input type="email" class="form-control" placeholder="Ingresa tu dirección de correo..." name="email" value="{{ old('email') }}" required></div>
                            <div class="form-group"><input type="password" class="form-control" placeholder="Ingresa tu contraseña..." name="password" required></div>
                            <div class="form-group"><input type="password" class="form-control" placeholder="Repite tu contraseña..." name="password_confirmation" required></div>
                            <div class="form-group form-group--submit"><button type="submit" class="btn btn-primary btn-block">Crear cuenta</button></div>
                        </form>
                    </div>
                    <div class="modal-account__item">
                        <form method="POST" action="{{ route('login') }}" class="modal-form">
                            <h5>Ingresa en tu cuenta</h5>
                            @csrf
                            <div class="form-group"><input type="email" name="email" class="form-control" placeholder="Ingresa tu dirección de correo..." value="{{ old('email') }}" required autofocus></div>
                            <div class="form-group"><input type="password" name="password" class="form-control" placeholder="Ingresa tu contraseña..." required></div>
                            <div class="form-group form-group--pass-reminder"><label class="checkbox checkbox-inline">
                              <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame <span class="checkbox-indicator"></span></label>
                              @if (Route::has('password.request'))
                                  <a href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
                              @endif
                            </div>
                            @if(AppHelper::site()->setting('site.recaptcha.active') == 1)
                              <div class="g-recaptcha" data-sitekey="{{AppHelper::site()->setting('site.recaptcha.site_key')}}"></div>
                            @endif
                            <div class="form-group form-group--submit"><button type="submit" class="btn btn-primary-inverse btn-block">Ingresar</button></div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

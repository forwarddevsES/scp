@if (count($breadcrumbs))
    <div class="page-heading">
      <div class="container">
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <h1 class="page-heading__title"><span class="highlight">{{ ($breadc = Breadcrumbs::current()) ? $breadc->title : 'Fallback Title' }}</span></h1>
            <ol class="page-heading__breadcrumb breadcrumb">
              @foreach ($breadcrumbs as $breadcrumb)

                  @if ($breadcrumb->url && !$loop->last)
                      <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                  @else
                      <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumb->title }}</li>
                  @endif

              @endforeach
            </ol>
          </div>
        </div>
      </div>
    </div>
@endif

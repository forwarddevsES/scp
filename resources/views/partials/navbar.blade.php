<div class="header-mobile clearfix" id="header-mobile">
    <div class="header-mobile__logo"><a href="{{url('/')}}"><img src="{{asset('assets\images\logo.png')}}" srcset="{{asset('assets\images\logo@2x.png')}} 2x" alt="Alchemists" class="header-mobile__logo-img"></a></div>
    <div class="header-mobile__inner"><a id="header-mobile__toggle" class="burger-menu-icon"><span class="burger-menu-icon__line"></span></a> <span class="header-mobile__search-icon" id="header-mobile__search-icon"></span></div>
</div>
<header class="header">
    <div class="header__top-bar clearfix">
        <div class="container">
            <ul class="nav-account">
              @auth
                <li class="nav-account__item"><a href="#"><span class="highlight">{{auth()->user()->name}}</span></a>
                    <ul class="main-nav__sub">
                        <li><a href="#"><i class="fa fa-user"></i> <span class="text-secondary">Cuenta</span></a></li>
                        <hr>
                        <li><a href="#">Soporte</a></li>
                        <hr>
                        <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form></li>
                    </ul>
                </li>
              @else
                <li class="nav-account__item"><a href="#" data-toggle="modal" data-target="#loginModal">Login</a></li>
              @endauth
            </ul>
        </div>
    </div>

    <div class="header__secondary">
        <div class="container">
            <div class="header-search-form">
                <form action="#" id="mobile-search-form" class="search-form"><input type="text" class="form-control header-mobile__search-control" value="" placeholder="{{AppHelper::site()->lang('search')}}"> <button type="submit"
                        class="header-mobile__search-submit"><i class="fa fa-search"></i></button></form>
            </div>
            <ul class="info-block info-block--header">
                <li class="info-block__item info-block__item--contact-primary"><svg role="img" class="df-icon df-icon--jersey">
                        <use xlink:href="assets/images/icons-basket.svg#jersey"></use>
                    </svg>
                    <h6 class="info-block__heading">{{AppHelper::site()->lang('play')}}</h6><a class="info-block__link" href="samp://{{AppHelper::site()->setting('site.server_ip')}}:{{AppHelper::site()->setting('site.server_port')}}">{{AppHelper::site()->setting('site.server_ip')}}:{{AppHelper::site()->setting('site.server_port')}}</a>
                </li>
                <li class="info-block__item info-block__item--contact-secondary"><svg role="img" class="df-icon df-icon--basketball">
                        <use xlink:href="assets/images/icons-basket.svg#basketball"></use>
                    </svg>
                    <h6 class="info-block__heading">{{AppHelper::site()->lang('contact')}}</h6><a class="info-block__link" href="mailto:{{AppHelper::site()->setting('site.contact_email')}}">{{AppHelper::site()->setting('site.contact_email')}}</a>
                </li>

            </ul>
        </div>
    </div>

    <div class="header__primary">
        <div class="container">
            <div class="header__primary-inner">
                <!-- Header Logo -->
                <div class="header-logo"><a href="{{url('/')}}"><img src="{{asset('assets\images\logo.png')}}" alt="Alchemists" srcset="assets\images\logo@2x.png 2x" class="header-logo__img"></a></div><!-- Header Logo / End -->
                <!-- Main Navigation -->
                <nav class="main-nav clearfix">
                    <ul class="main-nav__list">
                        <li class="{{AppHelper::site()->isroute('index')}}"><a href="{{url('/')}}">{{AppHelper::site()->lang('menu.index')}}</a></li>
                        <li class=""><a href="#">Features</a>
                            <div class="main-nav__megamenu clearfix">
                                <ul class="col-lg-2 col-md-3 col-12 main-nav__ul">
                                    <li class="main-nav__title">Features</li>
                                    <li><a href="{{ url('/') }}">Shortcodes</a></li>
                                    <li><a href="{{ url('/') }}">Typography</a></li>
                                    <li><a href="{{ url('/') }}">Widgets - Blog</a></li>
                                    <li><a href="{{ url('/') }}">Widgets - Shop</a></li>
                                    <li><a href="{{ url('/') }}">Widgets - Sports</a></li>
                                    <li><a href="{{ url('/') }}">404 Error Page</a></li>
                                    <li><a href="{{ url('/') }}">Search Results</a></li>
                                    <li><a href="{{ url('/') }}">Contact Us</a></li>
                                </ul>
                                <ul class="col-lg-2 col-md-3 col-12 main-nav__ul">
                                    <li class="main-nav__title">Main Features</li>
                                    <li><a href="{{ url('/') }}">Team Overview</a></li>
                                    <li><a href="{{ url('/') }}">Team Roster</a></li>
                                    <li><a href="{{ url('/') }}">Staff Member</a></li>
                                    <li><a href="{{ url('/') }}">Shop Page V1</a></li>
                                    <li><a href="{{ url('/') }}">Shop Page V2</a></li>
                                    <li><a href="{{ url('/') }}">Shopping Cart</a></li>
                                    <li><a href="{{ url('/') }}">Wishlist</a></li>
                                    <li><a href="{{ url('/') }}">Checkout</a></li>
                                </ul>
                                <div class="col-lg-4 col-md-3 col-12">
                                    <div class="posts posts--simple-list posts--simple-list--lg">
                                        <div class="posts__item posts__item--category-1">
                                            <div class="posts__inner">
                                                <div class="posts__cat"><span class="label posts__cat-label">The Team</span></div>
                                                <h6 class="posts__title"><a href="#">The team is starting a new power breakfast regimen</a></h6><time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
                                                <div class="posts__excerpt">Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                            </div>
                                            <div class="posts__footer card__footer">
                                                <div class="post-author">
                                                    <figure class="post-author__avatar"><img src="{{asset('assets\images\samples\avatar-1.jpg')}}" alt="Post Author Avatar"></figure>
                                                    <div class="post-author__info">
                                                        <h4 class="post-author__name">James Spiegel</h4>
                                                    </div>
                                                </div>
                                                <ul class="post__meta meta">
                                                    <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like meta-like--active icon-heart"></i> 530</a></li>
                                                    <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-3 col-12">
                                    <ul class="posts posts--simple-list">
                                        <li class="posts__item posts__item--category-1">
                                            <figure class="posts__thumb"><a href="#"><img src="{{asset('assets\images\samples\post-img3-xs.jpg')}}" alt=""></a></figure>
                                            <div class="posts__inner">
                                                <div class="posts__cat"><span class="label posts__cat-label">The Team</span></div>
                                                <h6 class="posts__title"><a href="#">The new eco friendly stadium won a Leafy Award in 2016</a></h6><time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                                            </div>
                                        </li>
                                        <li class="posts__item posts__item--category-2">
                                            <figure class="posts__thumb"><a href="#"><img src="{{asset('assets\images\samples\post-img1-xs.jpg')}}" alt=""></a></figure>
                                            <div class="posts__inner">
                                                <div class="posts__cat"><span class="label posts__cat-label">Injuries</span></div>
                                                <h6 class="posts__title"><a href="#">Mark Johnson has a Tibia Fracture and is gonna be out</a></h6><time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                                            </div>
                                        </li>
                                        <li class="posts__item posts__item--category-1">
                                            <figure class="posts__thumb"><a href="#"><img src="{{asset('assets\images\samples\post-img4-xs.jpg')}}" alt=""></a></figure>
                                            <div class="posts__inner">
                                                <div class="posts__cat"><span class="label posts__cat-label">The Team</span></div>
                                                <h6 class="posts__title"><a href="#">The team is starting a new power breakfast regimen</a></h6><time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class=""><a href="#">The Team</a>
                            <ul class="main-nav__sub">
                                <li><a href="{{ url('/') }}">Overview</a></li>
                                <li><a href="{{ url('/') }}">Roster</a>
                                    <ul class="main-nav__sub-2">
                                        <li><a href="{{ url('/') }}">Roster - 1</a></li>
                                        <li><a href="{{ url('/') }}">Roster - 2</a></li>
                                        <li><a href="{{ url('/') }}">Roster - 3 &nbsp; <span class="label label-danger">New</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/') }}">Standings</a></li>
                                <li><a href="{{ url('/') }}">Latest Results</a></li>
                                <li><a href="{{ url('/') }}">Schedule</a></li>
                                <li><a href="{{ url('/') }}">Gallery</a>
                                    <ul class="main-nav__sub-2">
                                        <li><a href="{{ url('/') }}">Single Album</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/') }}">Player Pages</a>
                                    <ul class="main-nav__sub-2">
                                        <li><a href="{{ url('/') }}">Overview</a></li>
                                        <li><a href="{{ url('/') }}">Full Statistics</a></li>
                                        <li><a href="{{ url('/') }}">Biography</a></li>
                                        <li><a href="{{ url('/') }}">Related News</a></li>
                                        <li><a href="{{ url('/') }}">Gallery</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/') }}">Staff Member</a></li>
                            </ul>
                        </li>
                        <li class=""><a href="#">News</a>
                            <ul class="main-nav__sub">
                                <li class=""><a href="{{ url('/') }}">News - version 1</a></li>
                                <li class=""><a href="{{ url('/') }}">News - version 2</a></li>
                                <li class=""><a href="{{ url('/') }}">News - version 3</a></li>
                                <li class=""><a href="{{ url('/') }}">News - version 4</a></li>
                                <li><a href="#">Post</a>
                                    <ul class="main-nav__sub-2">
                                        <li><a href="{{ url('/') }}">Single Post - version 1</a></li>
                                        <li><a href="{{ url('/') }}">Single Post - version 2</a></li>
                                        <li><a href="{{ url('/') }}">Single Post - version 3</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class=""><a href="{{ url('/') }}">Shop</a>
                            <ul class="main-nav__sub">
                                <li class=""><a href="{{ url('/') }}">Shop - Grid</a></li>
                                <li class=""><a href="{{ url('/') }}">Shop - List</a></li>
                                <li class=""><a href="{{ url('/') }}">Shop - Full Width</a></li>
                                <li class=""><a href="{{ url('/') }}">Single Product</a></li>
                                <li class=""><a href="{{ url('/') }}">Shopping Cart</a></li>
                                <li class=""><a href="{{ url('/') }}">Checkout</a></li>
                                <li class=""><a href="{{ url('/') }}">Wishlist</a></li>
                                <li class=""><a href="{{ url('/') }}">Login</a></li>
                                <li class=""><a href="{{ url('/') }}">Account</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="social-links social-links--inline social-links--main-nav">
                      @if (AppHelper::site()->setting('site.facebook.active') == 1)
                        <li class="social-links__item"><a href="{{AppHelper::site()->setting('site.facebook.url')}}" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa fa-facebook"></i></a></li>
                      @endif
                      @if (AppHelper::site()->setting('site.twitter.active') == 1)
                        <li class="social-links__item"><a href="{{AppHelper::site()->setting('site.twitter.url')}}" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa fa-twitter"></i></a></li>
                      @endif
                      @if (AppHelper::site()->setting('site.instagram.active') == 1)
                        <li class="social-links__item"><a href="{{AppHelper::site()->setting('site.instagram.url')}}" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa fa-instagram"></i></a></li>
                      @endif
                    </ul>
                    <!-- Pushy Panel Toggle --> <a href="#" class="pushy-panel__toggle"><span class="pushy-panel__line"></span> </a><!-- Pushy Panel Toggle / Eng -->
                </nav><!-- Main Navigation / End -->
            </div>
        </div>
    </div><!-- Header Primary / End -->
</header><!-- Header / End -->

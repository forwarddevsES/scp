<aside class="pushy-panel">
    <div class="pushy-panel__inner">
        <header class="pushy-panel__header">
            <div class="pushy-panel__logo"><a href="{{url('/')}}"><img src="{{asset('assets\images\logo.png')}}" srcset="assets\images\logo@2x.png 2x" alt="Alchemists"></a></div>
        </header>
        <div class="pushy-panel__content">
            <aside class="widget widget--side-panel">
                <div class="widget__content">
                  <div class="alert alert-success">
                    <b>{{AppHelper::site()->setting('site.server_ip')}}:{{AppHelper::site()->setting('site.server_port')}}</b>
                  </div>
                </div>
            </aside>
            @if (AppHelper::site()->setting('announce.active') == 1)
            <aside class="widget card widget--sidebar widget-banner">
              <div class="widget__title card__header">
                  <h4>Anuncios</h4>
              </div>
              <div class="widget__content card__content">
                  <figure class="widget-banner__img">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                      </ol>
                      <div class="carousel-inner">
                        @foreach ($advertising as $ad)
                          @if ($ad->active == 1)
                            <div class="carousel-item active">
                              <a href="{{ $ad->link }}">
                                <img class="d-block w-100" src="{{ $ad->image }}">
                              </a>
                            </div>
                          @else
                            <div class="carousel-item">
                              <a href="{{ $ad->link }}">
                                <img class="d-block w-100" src="{{ $ad->image }}">
                              </a>
                            </div>
                          @endif
                        @endforeach
                      </div>
                    </div>
                  </figure>
              </div>
            </aside>
            @endif

        </div><a href="#" class="pushy-panel__back-btn"></a>
    </div>
</aside>

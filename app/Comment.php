<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $fillable = [
      'user_id', 'comment', 'notice_id',
  ];
  public function user()
  {
    return $this->belongsTo('App\User');
  }
  public function notice()
  {
    return $this->belongsTo('App\Notice');
  }
}

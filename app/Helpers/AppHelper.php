<?php
namespace App\Helpers;
use App\Config;
use App\Language;
use Illuminate\Support\Facades\Route;
class AppHelper
{
      public function setting($name)
      {
        $exists = Config::where('key', $name)->count();
        if ($exists) {
          $config = Config::where('key', $name)->first();
          return $config->value;
        }
        else {
          return $name;
        }
      }
      public function isroute($name)
      {
        $routename = Route::currentRouteName();
        $route = Route::current();

        if ($routename == $name || $route == $name) {
          return 'active';
        }
        else {
          return '';
        }
      }
      public function lang($name)
      {
        $exists = Language::where('key', $name)->count();
        if ($exists) {
          $config = Language::where('key', $name)->first();
          return $config->value;
        }
        else {
          return $name;
        }
      }
      public static function site()
      {
        return new AppHelper();
      }
}

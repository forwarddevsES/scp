<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
  protected $fillable = [
      'user_id', 'title', 'content', 'image', 'category'
  ];
  public function user()
  {
    return $this->belongsTo('App\User');
  }
  public function comments()
  {
    return $this->hasMany('App\Comment');
  }
  public function categoryName()
  {
    switch ($this->category) {
      case 1:
        return 'Servidor';
        break;
      case 2:
        return 'Información';
        break;
      case 3:
        return 'Notas de Versión';
        break;
      case 4:
        return 'Web';
        break;

      default:
        return 'Servidor';
        break;
    }
  }
}

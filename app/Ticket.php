<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
  protected $fillable = [
      'user_id', 'title', 'content', 'state',
  ];

  public function messages()
  {
    return $this->hasMany('App\SupportMessage');
  }
}

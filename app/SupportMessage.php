<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportMessage extends Model
{
  protected $fillable = [
      'type', 'ticket_id', 'user_id', 'report_id', 'message',
  ];
  public function ticket()
  {
    return $this->belongsTo('App\Ticket');
  }
  public function report()
  {
    return $this->belongsTo('App\Report');
  }
  public function user()
  {
    return $this->belongsTo('App\Report');
  }
}

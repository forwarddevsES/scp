<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	protected $fillable = [
		'id', 'owner_id', 'type', 'price', 'label_x', 'label_y', 'label_z', 'label_out_x', 'label_out_y', 'label_out_z', 'x', 'y', 'z', 'interior', 'vw'
	];
	public function owner()
	{
		return $this->belongsTo('App\User');
	}
}

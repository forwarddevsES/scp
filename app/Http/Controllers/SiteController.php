<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;
use App\Comment;
use App\Advertising;

class SiteController extends Controller
{
    public function index()
    {
      $notice = Notice::orderBy('id', 'desc')->first();
      $others = Notice::where('category', $notice->category)->where('id', '!=', $notice->id)->orderBy('id', 'desc')->take(2)->get();
      $comments = Comment::where('notice_id', $notice->id)->orderBy('created_at', 'desc')->paginate(5);
      $advertising = Advertising::all();
      return view('site.index', ['notice' => $notice, 'others' => $others, 'comments' => $comments, 'advertising' => $advertising]);
    }
}

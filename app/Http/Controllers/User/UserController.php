<?php

namespace App\Http\Controllers\User;

use App\Advertising;
use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth']);
	}
	public function index()
	{
		$user = User::where('name', auth()->user()->name)->first();
		$advertising = Advertising::all();
		return view('site.user.index', ['user' => $user, 'advertising' => $advertising]);
	}
}

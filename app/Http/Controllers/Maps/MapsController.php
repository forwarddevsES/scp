<?php

namespace App\Http\Controllers\Maps;

use App\Advertising;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class MapsController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth']);
	}
	public function index()
	{
		$user = User::where('name', auth()->user()->name)->first();
		$advertising = Advertising::all();
		return view('site.maps.index', ['user' => $user, 'advertising' => $advertising]);
	}
}

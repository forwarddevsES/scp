<?php

namespace App\Http\Controllers\Admin;

use App\Advertising;
use App\Http\Controllers\Controller;
use App\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth']);
    }
    public function index()
    {
      $notices = Notice::orderBy('created_at', 'desc')->paginate(5);
      $advertising = Advertising::all();
      return view('site.admin.notices.index', ['notices' => $notices, 'advertising' => $advertising]);
    }
    public function create()
    {
      return view('site.admin.notices.create');
    }

}

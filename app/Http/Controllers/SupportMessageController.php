<?php

namespace App\Http\Controllers;

use App\SupportMessage;
use Illuminate\Http\Request;

class SupportMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupportMessage  $supportMessage
     * @return \Illuminate\Http\Response
     */
    public function show(SupportMessage $supportMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupportMessage  $supportMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportMessage $supportMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupportMessage  $supportMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportMessage $supportMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupportMessage  $supportMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportMessage $supportMessage)
    {
        //
    }
}

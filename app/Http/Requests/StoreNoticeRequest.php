<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNoticeRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function __construct()
    {
        $this->add(['user_id' => auth()->user()->id]);
    }

    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'image' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'Ocurrió un error en el sistema, contacte con el desarrollador.',
            'title.required' => 'El :attribute es obligatorio.',
            'content.required' => 'Añade un :attribute al producto.',
            'image.required' => 'El :attribute debe ser mínimo 0.'
        ];
    }


    public function attributes()
    {
        return [
            'title' => 'titulo de la noticia',
            'content' => 'contenido de la noticia',
            'image' => 'imagen de la noticia',
        ];
    }
}

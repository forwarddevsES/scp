<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password', 'level', 'money', 'kills', 'deads',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function notices()
    {
      return $this->hasMany('App\Notice');
    }
    public function comments()
    {
      return $this->hasMany('App\Comment');
    }
    public function messages()
    {
      return $this->hasMany('App\SupportMessage');
    }
    public function reports()
    {
      return $this->hasMany('App\Report');
    }
    public function tickets()
    {
      return $this->hasMany('App\Ticket');
    }
}

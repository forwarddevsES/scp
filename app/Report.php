<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
  protected $fillable = [
      'user_id', 'reported_id', 'rule_id', 'content',
  ];
  public function messages()
  {
    return $this->hasMany('App\SupportMessage');
  }
  public function rule()
  {
    return $this->belongsTo('App\Rule');
  }
  public function user()
  {
    return $this->belongsTo('App\User');
  }
  public function reported()
  {
    return $this->belongsTo('App\User');
  }
}

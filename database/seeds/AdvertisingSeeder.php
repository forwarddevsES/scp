<?php

use Illuminate\Database\Seeder;
use App\Advertising;

class AdvertisingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
    		array(
    			'image' => '/assets/images/samples/banner.jpg',
                'link' => '/',
                'active' => 1
			),
    		array(
    			'image' => '/assets/images/samples/banner.jpg',
                'link' => '/',
                'active' => 0
			),
    		array(
    			'image' => '/assets/images/samples/banner.jpg',
                'link' => '/',
                'active' => 0
			)
    	);
    	foreach ($data as $not) {
    		Advertising::create($not);
    	}
    }
}

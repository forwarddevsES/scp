<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
          array(
            'language' => 'es',
            'key' => 'title.index',
            'value' => 'Inicio'
          ),
          array(
            'language' => 'es',
            'key' => 'search',
            'value' => 'Ingresa tu busqueda aquí...'
          ),
          array(
            'language' => 'es',
            'key' => 'play',
            'value' => '¡Ingresa al servidor!'
          ),
          array(
            'language' => 'es',
            'key' => 'contact',
            'value' => 'Contacto'
          ),
          array(
            'language' => 'es',
            'key' => 'menu.index',
            'value' => 'Inicio'
          ),

        );
        Language::insert($data);
    }
}

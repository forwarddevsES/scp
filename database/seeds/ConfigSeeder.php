<?php

use Illuminate\Database\Seeder;
use App\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = array(
        array(
          'key' => 'site.name',
          'name' => 'Nombre del sitio',
          'description' => 'El nombre de su servidor o sitio web, este aparecerá en lugares como el titulo del sitio.',
          'value' => 'SCP'
        ),
        array(
          'key' => 'site.description',
          'name' => 'Descripción del sitio',
          'description' => 'Una breve descripción del sitio para ayudar a los buscadores a encontrar tu web',
          'value' => 'PCU hecho con Simple Control Panel por ForwardDevs.'
        ),
        array(
          'key' => 'site.keywords',
          'name' => 'Palabras Clave',
          'description' => 'Palabras que identifican tu sitio separadas por un espacio, esto ayudará el SEO de tu página.',
          'value' => 'PCU hecho con Simple Control Panel por ForwardDevs.'
        ),
        array(
          'key' => 'header.active',
          'name' => 'Imagen Principal',
          'description' => 'Activa o desactiva la imagen principal del inicio',
          'value' => '1'
        ),
        array(
          'key' => 'header.subtitle',
          'name' => 'Subtitulo',
          'description' => 'Subtitulo mediano situado debajo de las estrellas',
          'value' => 'FORWARDDEVS PRESENTA'
        ),
        array(
          'key' => 'header.title',
          'name' => 'Titulo',
          'description' => 'Titulo grande que se encuentra debajo del subtitulo',
          'value' => 'S'
        ),
        array(
          'key' => 'header.titlecolor',
          'name' => 'Titulo (Color Principal)',
          'description' => 'Titulo grande que se encuentra debajo del subtitulo en color principal',
          'value' => 'CP'
        ),
        array(
          'key' => 'header.description',
          'name' => 'Descripción',
          'description' => 'Es la ultima linea con un texto pequeño.',
          'value' => 'Simple Control Panel, un PCU diferente a los demás.'
        ),
        array(
          'key' => 'header.button.active',
          'name' => 'Boton',
          'description' => 'Activa o desactiva el botón situado abajo de la descripción',
          'value' => '1'
        ),
        array(
          'key' => 'header.button.text',
          'name' => 'Texto del boton',
          'description' => 'Texto que contendrá el boton.',
          'value' => 'Ingresar'
        ),
        array(
          'key' => 'header.button.href',
          'name' => 'Redirección del boton',
          'description' => 'URL a donde se redireccionará al apretar el botón.',
          'value' => '/login'
        ),
        array(
          'key' => 'site.recaptcha.active',
          'name' => 'Recaptcha',
          'description' => 'Activa la verificación recaptcha en el login.',
          'value' => '1'
        ),
        array(
          'key' => 'site.recaptcha.site_key',
          'name' => 'Clave del sitio (KEY)',
          'description' => 'Clave del sitio proporcionada por google.',
          'value' => '6Le5nZYUAAAAAFOnO_MnON930_Mn9e7jwl3NzJxd'
        ),
        array(
          'key' => 'site.recaptcha.secret_key',
          'name' => 'Clave secreta (SECRET_KEY)',
          'description' => 'Clave secreta proporcionada por google.',
          'value' => '6Le5nZYUAAAAAOgKfStCncwpiTbPEctogi5QGj5G'
        ),
        array(
          'key' => 'site.server_ip',
          'name' => 'IP del servidor',
          'description' => 'IP del servidor',
          'value' => '127.0.0.1'
        ),
        array(
          'key' => 'site.server_port',
          'name' => 'Puerto del servidor',
          'description' => 'Puerto del servidor',
          'value' => '7777'
        ),
        array(
          'key' => 'site.contact_email',
          'name' => 'Email de contacto',
          'description' => 'Email de contacto del administrador.',
          'value' => 'info@forwarddevs.com'
        ),
        array(
          'key' => 'site.facebook.active',
          'name' => 'Facebook',
          'description' => 'Activa o desactiva el contacto por Facebook',
          'value' => '1'
        ),
        array(
          'key' => 'site.twitter.active',
          'name' => 'Twitter',
          'description' => 'Activa o desactiva el contacto por Twitter',
          'value' => '1'
        ),
        array(
          'key' => 'site.instagram.active',
          'name' => 'Instagram',
          'description' => 'Activa o desactiva el contacto por Instagram',
          'value' => '1'
        ),
        array(
          'key' => 'site.facebook.url',
          'name' => 'URL Facebook',
          'description' => 'URL de Facebook',
          'value' => 'https://www.facebook.com'
        ),
        array(
          'key' => 'site.twitter.url',
          'name' => 'URL Twitter',
          'description' => 'URL de Twitter',
          'value' => 'https://www.twitter.com'
        ),
        array(
          'key' => 'site.instagram.url',
          'name' => 'URL Instagram',
          'description' => 'URL de Instagram',
          'value' => 'https://www.instagram.com'
        ),
        array(
          'key' => 'announce.active',
          'name' => 'Anuncio de publicidad',
          'description' => 'Activar o desactivar anuncio de publicidad',
          'value' => '1'
        ),
        array(
          'key' => 'announce.url',
          'name' => 'URL de publicidad',
          'description' => 'URL hacia donde redirigirá la publicidad.',
          'value' => 'https://www.forwarddevs.com'
        ),
        array(
          'key' => 'announce.image',
          'name' => 'Imagen de publicidad',
          'description' => 'Imagen de la publicidad (300x250).',
          'value' => 'http://localhost:8000/assets/images/samples/banner.jpg'
        ),
      );
        Config::insert($data);
    }
}

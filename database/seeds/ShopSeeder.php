<?php

use App\Shop;
use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array(
    		array(
    			'owner_id' => '1',
    			'type' => '1',
    			'price' => '1580000',
    			'label_x' => '809.005',
    			'label_y' => '-1349.23',
    			'label_z' => '13.5414',
    			'label_out_x' => '809.903',
    			'label_out_y' => '-1349.32',
    			'label_out_z' => '13.5398',
    			'x' => '809.005',
    			'y' => '-1349.23',
    			'z' => '13.5414',
    		),
    		array(
    			'owner_id' => '1',
    			'type' => '2',
    			'price' => '18900000',
    			'label_x' => '809.903',
    			'label_y' => '-1349.32',
    			'label_z' => '13.5398',
    			'label_out_x' => '809.005',
    			'label_out_y' => '-1349.23',
    			'label_out_z' => '13.5414',
    			'x' => '809.903',
    			'y' => '-1349.32',
    			'z' => '13.5398',
    		);
    	);
    	foreach ($data as $not) {
    		Shop::create($not);
    	}
    }
}

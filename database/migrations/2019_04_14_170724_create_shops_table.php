<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('owner_id');
            $table->integer('type')->default('1');
            $table->unsignedInteger('price');
            $table->float('label_x');
            $table->float('label_y');
            $table->float('label_z');
            $table->float('label_out_x');
            $table->float('label_out_y');
            $table->float('label_out_z');
            $table->float('x');
            $table->float('y');
            $table->float('z');
            $table->integer('interior')->default('0');
            $table->integer('vw')->default('0');

            $table->foreign('owner_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
